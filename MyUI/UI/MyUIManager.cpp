#include "MyUIManager.h"

MyUIManager::MyUIManager()
: mouseState(MS_IDLE), oldMouseState(MS_IDLE), mousePos(-1, -1),
keyState(KS_IDLE),
num(0),
isWinMode(false)
{
	for(int i = 0 ; i < 4 ; ++i)
	{
		isLeftDowned[i] = false;
		isLeftClicked[i] = false;
	}
}
MyUIManager::MyUIManager(const MyUIManager&)
{}
MyUIManager::~MyUIManager()
{}
MyUIManager&
MyUIManager::operator = (const MyUIManager&)
{
	return *this;
}
void
MyUIManager::setWindowMode(bool _bWindow)
{
	isWinMode = _bWindow;
}
bool
MyUIManager::isWindowMode()
{
	return isWinMode;
}
void
MyUIManager::sendMouseState(MouseState ms)
{
	mouseState = ms;
}
void
MyUIManager::sendMousePos(MousePos& mp)
{
	mousePos = mp;
}
void
MyUIManager::keyDownedInWndProc()
{
	if(keyState == KS_DOWN)
		keyState = KS_IDLE;
	else
		keyState = KS_DOWN;
}
MouseState
MyUIManager::getMouseState()
{
	if(mouseState == MS_LBUP || mouseState == MS_RBUP)	// 이전에 마우스를 떼었으면 지금은 중립이다.
	{
		mouseState = MS_IDLE;
	}
	return mouseState;
}
MousePos
MyUIManager::getMousePos()
{
	return mousePos;
}
bool
MyUIManager::isLeftMouseButtonClicked(RECT* rect, int _num)
{
	bool isInRect = false;
	if(rect != 0)
	{
		if(!isWindowMode())
		{
			if(mousePos.x >= rect->left - TESTXX &&
				mousePos.x <= rect->right - TESTXX &&
				mousePos.y >= rect->top - TESTYY &&
				mousePos.y <= rect->bottom - TESTYY)
			{
				isInRect = true;
			}
		}
		else if(mousePos.x >= rect->left / TESTX &&
			mousePos.x <= rect->right / TESTX &&
			mousePos.y >= rect->top / TESTY &&
			mousePos.y <= rect->bottom / TESTY)
		{
			isInRect = true;
		}
	}
	if(mouseState == MS_LBDOWN)
	{
		isLeftDowned[_num] = true;
	}
	else
	{
		if(isLeftDowned[_num])
		{
			isLeftClicked[_num] = true;
		}
		isLeftDowned[_num] = false;
	}
	if(isLeftClicked[_num])
	{
		isLeftClicked[_num] = false;
		if(rect)
			return isInRect;
		return true;
	}
	return false;
}
bool
MyUIManager::isAnyKeyDown()
{
	if(keyState == KS_DOWN)
	{
		keyState = KS_IDLE;
		return true;
	}
	return false;
}