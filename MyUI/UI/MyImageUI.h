#pragma once

#include "MyUIDef.h"
#include "MyUIManager.h"

class MyImageUI
{
protected:
	MyUIManager*		mgr;
	LPD3DXSPRITE		sprImage;
	LPDIRECT3DTEXTURE9	texImage;

	/* 09.02.24 */
	D3DXMATRIX			matScale;
	D3DXMATRIX			matRotate;
	D3DXMATRIX			matTranslate;

	int width;
	int height;
	RECT rectImage;

	bool isImgDown;
	bool isImgClicked;

public:
	// 생성자 및 소멸자
	MyImageUI();
	MyImageUI(const MyImageUI&);
	virtual ~MyImageUI();

	// 연산자
	MyImageUI& operator = (const MyImageUI&);

	// 이미지 초기화
	BOOL initImage(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, const TCHAR* nameImage, int _width, int _height);

	// 이미지 바꾸기
	BOOL changeImage(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, const TCHAR* nameImage, int _width, int _height);

	// 이미지 그리기(isBack을 true로 넘겨주면 3D에 가리도록 그림)
	BOOL drawImage(int x, int y, bool isBack = true);

	// 이미지 영역 안에 마우스가 있는가?
	BOOL isMouseOnImage(MousePos& pos);

	// 이미지 영역 안에 왼쪽 마우스 클릭이 일어났는가?
	BOOL isImageClicked();

	/* 09.02.24 */
	void scale(float sX, float sY);

	/* 09.02.24 */
	void rotate(float r);
};