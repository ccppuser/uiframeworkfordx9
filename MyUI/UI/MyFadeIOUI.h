#pragma once

#include "MyUIDef.h"
#include <iostream>

class MyFadeIOUI
{
protected:
	LPD3DXSPRITE		sprFade;
	LPDIRECT3DTEXTURE9	texFade;

	int					width;
	int					height;
	DWORD				alphaIn;
	DWORD				oldAlphaIn;
	DWORD				alphaOut;
	DWORD				oldAlphaOut;
	RECT				rectFade;
	float				timeLastIn;
	float				timeElapsedIn;
	float				timeLastOut;
	float				timeElapsedOut;
	bool				isFinishedIn;
	bool				isFinishedOut;

public:
	// 생성자 및 소멸자
	MyFadeIOUI();
	MyFadeIOUI(const MyFadeIOUI&);
	virtual ~MyFadeIOUI();

	// 연산자
	MyFadeIOUI& operator = (const MyFadeIOUI&);

	// 초기화
	BOOL initFade(LPDIRECT3DDEVICE9 device, int _width, int _height);

	// 페이드 인과 페이드 아웃의 시간/알파값 초기화
	VOID initFadeIn();
	VOID initFadeOut();

	// 페이드 인(speed : 페이드 인 속도)
	// 페이드 인이 끝나면 TRUE 리턴
	BOOL fadeIn(float speed = 1.0f);

	// 페이드 아웃(speed : 페이드 아웃 속도)
	// 페이드 아웃이 끝나면 FALSE 리턴
	BOOL fadeOut(float speed = 1.0f);
};