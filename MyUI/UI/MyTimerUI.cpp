#include "MyTimerUI.h"

MyTimerUI::MyTimerUI()
: timeOld(0.0f), timeElapsed(0.0f), isFirst(TRUE), isExpired(FALSE)
{}
MyTimerUI::MyTimerUI(const MyTimerUI&)
{}
MyTimerUI::~MyTimerUI()
{}
MyTimerUI&
MyTimerUI::operator = (const MyTimerUI&)
{
	return *this;
}
BOOL
MyTimerUI::initTimer()
{
	timeOld = 0.0f;
	timeElapsed = 0.0f;
	isFirst = TRUE;
	isExpired = FALSE;
	return TRUE;
}
BOOL
MyTimerUI::runTimer(float _offset)
{
	if(!isExpired)
	{
		float timeCurr = static_cast<float>(timeGetTime());
		float timeDelta = (timeCurr - timeOld) * 0.001f;
		if(timeDelta > 1.0f && !isFirst)
			timeDelta = 0.0f;
		timeElapsed += timeDelta;
		if(timeElapsed >= _offset)
		{
			timeElapsed = 0.0f;
			if(!isFirst)
			{
				isExpired = TRUE;
				return TRUE;
			}
			isFirst = FALSE;
		}
		timeOld = timeCurr;
		return FALSE;
	}
	else
		return TRUE;
}