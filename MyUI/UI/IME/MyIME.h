//
// IME 클래스
//

#pragma once

//#pragma comment(lib, "imm32")

//#define MAXSTRING 128

#include <d3d9.h>
#include <d3dx9.h>
#include <mmsystem.h>

#define SAFE_DELETE(x)			if(x){delete x; x = 0;}
#define SAFE_DELETE_ARRAY(x)	if(x){delete [] x; x = 0;}
#define SAFE_RELEASE(x)			if(x){x->Release(); x = 0;}

class CMyIME
{
private:
	HIMC		hIMC ;
	long		length ;

	char		compChar[3];

	long		index ;
	char		*pStr ;

	LOGFONT		logFont;

	int			numMaxChar;

public:
	// 생성자 및 소멸자
	CMyIME(void);
	CMyIME(const CMyIME&);
	~CMyIME(void);

	// IME 클래스 초기화(D3D디바이스, 글꼴 필요)
	// 초기화 실패하면 false 리턴
	bool	initMyIME(D3DXFONT_DESC& desc);

	// 한글 조합 문자를 다룸
	// WM_IME_COMPOSITION 에서 호출
	void	hangeulComposition	(HWND, WPARAM, LPARAM);

	// 한글 이외의 문자를 다룸
	// WM_CHAR 에서 호출
	void	asciiComposition	(HWND, WPARAM, LPARAM);

	// 한/영 변환을 지정함
	// WM_CREATE 에서 호출
	void	conversionMode		(HWND, BOOL);

	// 현재 입력된 문자열의 포인터를 리턴
	char*	getString			(void);

	// 현재 입력한 문자열의 길이를 구함(실제 윈도우 화면 상 길이)
	long	getLength			(void);

	// 문자 최대 입력 개수 정의
	void	setNumMaxChar(int _num);
};