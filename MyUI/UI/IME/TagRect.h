#pragma once

#include <stdio.h>

template <typename T>
struct tagRect
{
	tagRect()
		:x(0), y(0), w(0), h(0) {}
	tagRect(const T &_x, const T &_y, const T &_w, const T &_h)
		:x(_x), y(_y), w(_w), h(_h) {}
	tagRect(const tagRect &_Rect)
		:x(_Rect.x), y(_Rect.y), w(_Rect.w), h(_Rect.h){}
	~tagRect() {}

	tagRect &operator=(const tagRect<T> &rhs)
	{
		x = rhs.x;
		y = rhs.y;
		w = rhs.w;
		h = rhs.h;

		return (*this);
	}
	T x;
	T y;
	T w;
	T h;
};

template <typename T>
struct tagPosition
{
	tagPosition()
		:x(0), y(0) {}
	tagPosition(const T &_x, const T &_y)
		:x(_x), y(_y) {}
	tagPosition(const tagPosition &rhs)
		:x(rhs.x), y(rhs.y){}
	~tagPosition() {}
	T x;
	T y;

	tagPosition &operator=(const tagPosition &rhs)
	{
		x = rhs.x;
		y = rhs.y;

		return (*this);
	}
	bool operator==(const tagPosition &rhs)
	{
		if(x == rhs.x)
		{
			if(y == rhs.y)
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	bool operator!=(const tagPosition &rhs)
	{
		if(x == rhs.x)
		{
			if(y == rhs.y)
			{
				return false;
			}
			else
				return true;
		}
		else
			return true;
	}
};