#pragma once

//#pragma comment(lib, "imm32")

//////////////////////////////////////////////////////////////////////////////////////////
//		�� : 479									//////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

#include <TCHAR.h>
#include <windows.h>
#include <stdlib.h>
#include <string>
#include <cassert>

#ifdef _UNICODE
typedef std::wstring String;
#else
typedef std::string String;
#endif

template<typename T>
void CreateSingletonInstance()
{
	new T;
}

template<typename T>
void DeleteSingletonInstance()
{
	if(T::Instance)
	{
		delete T::Instance;
		T::Instance = 0;
	}
}

template<typename T>
class SingleTon
{
	friend void CreateSingletonInstance();
	friend void DeleteSingletonInstance();

protected:
	static T *Instance;

protected:
	SingleTon()
	{
		size_t offset = (T*)1 - (SingleTon<T>*)(T*)1 ;
		Instance = (T*)(this + offset);
	}
	virtual ~SingleTon()
	{}

public:
	static T *GetInstancePtr()
	{
		return Instance;
	}
	static bool empty()
	{
		return Instance ? false : true;
	}
};
template<typename T>
T* SingleTon<T>::Instance = 0;