#pragma once

template <typename T>
struct Tsize
{
	Tsize()
		:Width(0), Height(0){}
	~Tsize(){}
	Tsize(const T &_WIdth, const T &_Height)
		:Width(_WIdth), Height(_Height){}
	Tsize(const Tsize<T> &_size)
		:Width(_size.Width), Height(_size.Height){}
	Tsize &operator=(const Tsize<T> &rhs)
	{
		this->Width = rhs.Width;
		this->Height = rhs.Height;

		return (*this);
	}
	Tsize &operator+=(const Tsize<T> &rhs)
	{
		Width += rhs.Width;
		Height += rhs.Height;

		return (*this);
	}
	Tsize operator+(const Tsize<T> &rhs)
	{
		return Tsize<T>(Width+rhs.Width, Height+rhs.Height);
	}
	Tsize &operator-=(const Tsize<T> &rhs)
	{
		if(Width < rhs.Width)
			Swap(Width, rhs.Width);
		if(Height < rhs.Height)
			Swap(Height, rhs.Height);
		Width -= rhs.Width;
		Height -= rhs.Height;

		return (*this);
	}
	Tsize operator-(const Tsize<T> &rhs)
	{
		if(Width < rhs.Width)
			Swap(Width, rhs.Width);
		if(Height < rhs.Height)
			Swap(Height, rhs.Height);
		return Tsize<T>(Width-rhs.Width, Height-rhs.Height);
	}
	T Width;
	T Height;
};

template <typename T>
void Swap(T &lhs, T &rhs)
{
	T temp = lhs;
	lhs = rhs;
	rhs = temp;
}