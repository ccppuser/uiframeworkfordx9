#include "stdafx.h"
#include <Imm.h>
#include "MyIME.h"

CMyIME::CMyIME(void)
: index(0), numMaxChar(128)
{
	memset(compChar, 0, 3);
	pStr = new char[numMaxChar+3];
	memset(pStr, 0, numMaxChar+3);
}
CMyIME::~CMyIME(void)
{
	SAFE_DELETE_ARRAY(pStr);
}
bool
CMyIME::initMyIME(D3DXFONT_DESC& desc)
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));

	lf.lfCharSet		= desc.CharSet;
	lf.lfHeight			= desc.Height;
	lf.lfItalic			= desc.Italic;
	lf.lfOutPrecision	= desc.OutputPrecision;
	lf.lfPitchAndFamily	= desc.PitchAndFamily;
	lf.lfQuality		= desc.Quality;
	lf.lfWeight			= desc.Weight;
	lf.lfWidth			= desc.Width;
	logFont = lf;

	return true;
}
void
CMyIME::hangeulComposition(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	if( index >= numMaxChar )
		return ;

	hIMC = ImmGetContext(hWnd);

	if( lParam & GCS_RESULTSTR )
	{
		length = ImmGetCompositionString(hIMC, GCS_RESULTSTR, 0, 0);
		ImmGetCompositionString(hIMC, GCS_RESULTSTR, compChar, length);
		memcpy(&pStr[index], compChar, length);
		index += length;
//#if (defined UNICODE && defined _UNICODE)
//		index += length - 1;	// UNICODE
//#else
//		index += length;	// MBCS
//#endif
	}
	else
	{
		length = ImmGetCompositionString(hIMC, GCS_COMPSTR, 0, 0);
		ImmGetCompositionString(hIMC, GCS_COMPSTR, compChar, length);

		if(length != 0)
			memcpy(&pStr[index], compChar, length);
		else
			memset(&pStr[index], 0, 3);
	}

	ImmReleaseContext(hWnd, hIMC);
}
void
CMyIME::asciiComposition(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case VK_BACK:
		switch( index )
		{
		case 0:
			break;
		case 1:
			pStr[--index] = 0 ;
			break;
		default:
			if( ((pStr[index-1] & 0x80) == 0x80) && ((pStr[index-2]&0x80) == 0x80) )
			{
				pStr[--index] = 0;
				pStr[--index] = 0;
			}
			else
			{
				pStr[--index] = 0;
			}
			break;
		}
		break;
	case VK_RETURN:
		memset(pStr, 0, index);
		index = 0;
		break;
	default:
		if( index < numMaxChar+3-1 )
			pStr[index++] = static_cast<char>(wParam & 0xff);
		break;
	}
}
void
CMyIME::conversionMode(HWND hWnd, BOOL HangeulMode)
{
	DWORD dwConversion ;
	hIMC = ImmGetContext(hWnd);

	if( HangeulMode )
		dwConversion = IME_CMODE_HANGEUL ;
	else
		dwConversion = IME_CMODE_ALPHANUMERIC ;

	ImmSetConversionStatus(hIMC, dwConversion, IME_SMODE_NONE);
	ImmReleaseContext(hWnd, hIMC);
}
TCHAR*
CMyIME::getString(void)
{
	return pStr;
}
long
CMyIME::getLength(void)
{
	HWND hwnd = GetWindow(NULL, 0);
	HDC hdc = GetDC(hwnd);
	HFONT hFont = CreateFontIndirect(&logFont);
	HFONT holdFont;
	SIZE sz;
	long len;

	holdFont = (HFONT)SelectObject(hdc, hFont);

	// 문자열의 실제 윈도우상 길이를 구함
	GetTextExtentPoint32(hdc, pStr, (int)strlen(pStr), &sz);
	len = sz.cx;

	SelectObject(hdc, holdFont);
	DeleteObject(hFont);
	ReleaseDC(hwnd, hdc);

	return len;
}
void
CMyIME::setNumMaxChar(int _num)
{
	numMaxChar = _num;
}