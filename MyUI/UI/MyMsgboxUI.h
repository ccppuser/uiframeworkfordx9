#pragma once

#include "MyUIDef.h"
#include "MyUIManager.h"
#include "MyFrameUI.h"
#include "MyTextUI.h"
#include "MyButtonUI.h"

#include <Windows.h>

#define MAX_STRING	20
#define MAX_WEIGHT	1000

#define CLR_BOLD	0xffff0000
#define CLR_NORM	0xff777777

struct strInfo
{
	std::string str;
	float rowSize;
};

class MyMsgboxUI
	: public MyFrameUI, public MyTextUI, public MyButtonUI
{
protected:
	MyUIManager* mgr;
	int idxStr;
	strInfo m_strInfo[MAX_STRING];		// 10 줄 까지 등록 가능

	bool isBttnUsing;	// 버튼을 사용하는가
	HFONT font_normal;	// 간격구하기용 폰트
	HFONT font_bold;	// 간격구하기용 폰트

	LPDIRECT3DDEVICE9 device; //간격구할때 사용

public:
	// 생성자 및 소멸자
	MyMsgboxUI(MyUIManager* _mgr);
	MyMsgboxUI(const MyMsgboxUI&);
	virtual ~MyMsgboxUI();

	// 연산자
	MyMsgboxUI& operator = (const MyMsgboxUI&);

	// 메시지 박스 프레임 초기화
	// 인수 : 디바이스, 글꼴 구조체, 프레임 이미지 경로, 프레임 좌표, x, y, width, height
	//        버튼idle이미지경로, 버튼눌린이미지경로, x, y, width, height
	//        fontSizeRate: 1.0f는 기본 크기 0.1f ~ 크기 비율 지정
	// 버튼 이미지의 경로를 0이나 NULL로 지정하면 버튼 없는 메시지 박스 생성
	bool initMsgboxFrame(LPDIRECT3DDEVICE9 _device, D3DXFONT_DESC& _desc,
		TCHAR* frameImage, Frame& frm, int x, int y, int width, int height,
		const TCHAR* normBttnImage, const TCHAR* mouseOnImagePath, const TCHAR* clickedBttnImage,
		int bttnX, int bttnY, int bttnWidth, int bttnHeight, float fontSizeRate = 1.0f);

	// 메시지 박스 그리기
	bool drawMsgbox();
	bool drawBoldText(std::string _str, std::string _space, int i);
	bool drawNormalText(std::string _str, std::string _space, int i);
	bool drawComplexText(std::string _str, std::string bold_space, std::string nor_space, int i, D3DCOLOR color, float rowSize = 1.0f);

	// 메시지 등록
	bool registText(std::string _str, float _rowSize = 1.0f);

	// 메시지 등록 해제(모두)
	bool unRegistTexts();

	// 메시지 박스의 버튼이 눌렸는가?
	bool isButtonClicked();
};