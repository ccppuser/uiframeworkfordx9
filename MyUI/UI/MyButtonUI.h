#pragma once

#include <iostream>
#include "MyUIDef.h"
#include "MyUIManager.h"

class MyButtonUI
{
protected:
	MyUIManager*		mgr;
	LPD3DXSPRITE		sprButton;
	LPDIRECT3DTEXTURE9	texNormal;

	//LPD3DXSPRITE		sprMouseOn;
	LPDIRECT3DTEXTURE9	texMouseOn;

	//LPD3DXSPRITE		sprClicked;
	LPDIRECT3DTEXTURE9	texClicked;

	RECT rectButton;

	//bool selfManage;	// true로 하면 눌린 상태와 뗀 상태를 임의로 조종 가능

	bool isBttnDown;
	bool isBttnClicked;

public:
	// 생성자 및 소멸자
	MyButtonUI();
	MyButtonUI(const MyButtonUI&);
	virtual ~MyButtonUI();

	// 연산자
	MyButtonUI& operator = (const MyButtonUI&);

	// 버튼 초기화(이미지 경로(노말, 눌렸을 때))
	BOOL initButton(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr,
		const TCHAR* normalImagePath, const TCHAR* mouseOnImagePath, const TCHAR* clickedImagePath,
		int _x, int _y, int _width, int _height);

	// MyUI 매니져 등록
	virtual void registManager(MyUIManager* _mgr);

	// 버튼 그리기
	BOOL drawButton();
	BOOL drawNormalButton();
	BOOL drawMouseOnButton();
	BOOL drawClickedButton();

	// 버튼을 누른 상태인가?
	BOOL isMouseOnButton(MousePos& pos);

	// 버튼을 눌렀다 뗐는가?
	// 한번 눌렀다 떼면 계속 TRUE 리턴
	BOOL isButtonClicked();
};