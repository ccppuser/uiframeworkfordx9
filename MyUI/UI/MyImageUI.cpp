#include "MyImageUI.h"

MyImageUI::MyImageUI()
: mgr(0), sprImage(0), texImage(0), isImgDown(0), isImgClicked(0)
{}
MyImageUI::MyImageUI(const MyImageUI&)
{}
MyImageUI::~MyImageUI()
{
	SAFE_RELEASE(sprImage);
	SAFE_RELEASE(texImage);
}
MyImageUI&
MyImageUI::operator = (const MyImageUI&)
{
	return *this;
}
BOOL
MyImageUI::initImage(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, const TCHAR* nameImage, int _width, int _height)
{
	mgr = _mgr;
	width = _width;
	height = _height;

	/* 09.02.24 */
	D3DXMatrixIdentity(&matScale);
	D3DXMatrixIdentity(&matRotate);
	D3DXMatrixIdentity(&matTranslate);

	if(FAILED(D3DXCreateSprite(device, &sprImage)))
	{
		MessageBox(NULL, "failed to create sprite.", NULL, NULL);
		return FALSE;
	}
	if(FAILED(D3DXCreateTextureFromFileEx(device, nameImage, width, height, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT,
		D3DX_DEFAULT, 0, NULL, NULL, &texImage)))
	{
		MessageBox(NULL, "failed to create texture.", NULL, NULL);
		return FALSE;
	}
	return TRUE;
}
BOOL
MyImageUI::changeImage(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, const TCHAR* nameImage, int _width, int _height)
{
	SAFE_RELEASE(sprImage);
	SAFE_RELEASE(texImage);
	mgr = _mgr;
	width = _width;
	height = _height;
	if(FAILED(D3DXCreateSprite(device, &sprImage)))
	{
		MessageBox(NULL, "failed to create sprite.", NULL, NULL);
		return FALSE;
	}
	if(FAILED(D3DXCreateTextureFromFileEx(device, nameImage, D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT,
		D3DX_DEFAULT, 0, NULL, NULL, &texImage)))
	{
		MessageBox(NULL, "failed to create texture.", NULL, NULL);
		return FALSE;
	}
	return TRUE;
}
BOOL
MyImageUI::drawImage(int x, int y, bool isBack)
{
	/* 09.02.24 */
	rectImage.left = x; rectImage.top = y;
	rectImage.right = x + width; rectImage.bottom = y + height;
	RECT rect = {0, 0, width, height};
	D3DXVECTOR3 pos(0, 0, (isBack) ? 1.0f : 0.0f);
	D3DXMatrixTranslation(&matTranslate, x, y, 0.0f);
	sprImage->SetTransform(&(matScale * matRotate * matTranslate));
	sprImage->Begin(0);
		sprImage->Draw(texImage, NULL, NULL, &pos, 0xffffffff);
	sprImage->End();
	return TRUE;
}
BOOL
MyImageUI::isMouseOnImage(MousePos& pos)
{
	if(!mgr->isWindowMode())
	{
		if(pos.x >= rectImage.left - TESTXX &&
			pos.x <= rectImage.right - TESTXX &&
			pos.y >= rectImage.top - TESTYY &&
			pos.y <= rectImage.bottom - TESTYY)
		{
			return TRUE;
		}
	}
	else if(pos.x >= rectImage.left / TESTX &&
		pos.x <= rectImage.right / TESTX &&
		pos.y >= rectImage.top / TESTY &&
		pos.y <= rectImage.bottom / TESTY)
	{
		return TRUE;
	}
	return FALSE;
}
BOOL
MyImageUI::isImageClicked()
{
	if(mgr->getMouseState() == MS_LBDOWN && isMouseOnImage(mgr->getMousePos()))
	{
		isImgDown = true;
	}
	else
	{
		if(isImgDown && isMouseOnImage(mgr->getMousePos()))
		{
			isImgClicked = true;
		}
		isImgDown = false;
	}
	if(isImgClicked)
	{
		isImgClicked = false;
		return true;
	}
	return false;
}
void
MyImageUI::scale(float sX, float sY)
{
	D3DXMatrixScaling(&matScale, sX, sY, 1.0f);
}
void
MyImageUI::rotate(float r)
{
	D3DXMatrixRotationZ(&matRotate, r);
}