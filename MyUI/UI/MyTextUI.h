#pragma once

#include "MyUIDef.h"
#include "MyUIManager.h"
#include <string>

class MyTextUI
{
protected:
	MyUIManager*	mgr;
	LOGFONT			logFont;
	LPD3DXFONT		font;
	D3DXFONT_DESC	desc;

	RECT			rectText;

	std::string			str;

	bool			isTxtDown;
	bool			isTxtClicked;

public:
	// 생성자 및 소멸자
	MyTextUI();
	MyTextUI(const MyTextUI&);
	virtual ~MyTextUI();

	// 연산자
	MyTextUI& operator = (const MyTextUI&);

	// d3d디바이스와 d3d글씨 구조체 받음
	bool initText(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, D3DXFONT_DESC& _desc);

	// 현재 문자열을 그려줌(그릴 d3d스프라이트, 그릴 RECT 객체, rect에서의 그릴 위치, 글자색 필요)
	// BeginScene() 와 EndScene() 사이에서 호출해야 함(d3d디바이스)
	// format : DT_TOP | DT_LEFT | DT_RIGHT | DT_BOTTOM 등등
	bool drawText(String _str, LPD3DXSPRITE _spr, int x, int y, DWORD format, D3DCOLOR color);

	// 문자열의 윈도우상 실제 길이 구함
	long getLength();
	// 깜박이는 커서 그려줌(현재 입력중인 문자열 뒤에)
	// BeginScene() 와 EndScene() 사이에서 호출해야 함(d3d디바이스)
	//bool DrawCursor(LPDIRECT3DDEVICE9 _device, long len);

	// 마우스가 텍스트 위에 있는가
	bool isMouseOnText(MousePos& pos);

	// 텍스트가 눌렸는가?
	bool isTextClicked();

	D3DXFONT_DESC getDesc();
};