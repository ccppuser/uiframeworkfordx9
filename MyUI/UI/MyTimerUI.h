#pragma once

#include "MyUIDef.h"

class MyTimerUI
{
protected:
	float timeOld;
	float timeElapsed;

	BOOL isFirst;
	BOOL isExpired;

public:
	// 생성자 및 소멸자
	MyTimerUI();
	MyTimerUI(const MyTimerUI&);
	virtual ~MyTimerUI();

	// 연산자
	MyTimerUI& operator = (const MyTimerUI&);

	// 타이머 초기화
	BOOL initTimer();

	// 타이머 작동(offset에 지정한 시간(단위:초)이 되면 TRUE 리턴)
	BOOL runTimer(float _offset = 1.0f);
};