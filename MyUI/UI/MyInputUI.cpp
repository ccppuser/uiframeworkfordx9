#include "MyInputUI.h"

MyInputUI::MyInputUI(void)
: font(0), sprCursor(0), texCursor(0)
{
	currTime = static_cast<float>(timeGetTime());
	lastTime = currTime;
	//rect.left = 0; rect.top = 0;
	//rect.right = 200; rect.bottom = 30;
}
MyInputUI::MyInputUI(const MyInputUI& rhs)
{
}
MyInputUI::~MyInputUI(void)
{
	SAFE_RELEASE(font);
	SAFE_RELEASE(sprCursor);
	SAFE_RELEASE(texCursor);
}
MyInputUI&
MyInputUI::operator =(const MyInputUI &rhs)
{
	return *this;
}
bool
MyInputUI::initInputUI(LPDIRECT3DDEVICE9 _device, D3DXFONT_DESC& _desc)
{
	desc = _desc;
	if(!_device)
		return false;	// D3D디바이스 생성 안돼있으면 false

	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));

	strcpy_s(lf.lfFaceName, strlen(_desc.FaceName)+1, _desc.FaceName);
	lf.lfCharSet		= _desc.CharSet;
	lf.lfHeight			= _desc.Height;
	lf.lfItalic			= _desc.Italic;
	lf.lfOutPrecision	= _desc.OutputPrecision;
	lf.lfPitchAndFamily	= _desc.PitchAndFamily;
	lf.lfQuality		= _desc.Quality;
	lf.lfWeight			= _desc.Weight;
	lf.lfWidth			= _desc.Width;
	logFont = lf;

	// 커서를 출력할 스프라이트 만들기
	if(FAILED(D3DXCreateSprite(_device, &sprCursor)))
	{
		MessageBox(NULL, _T("failed to create sprite."), NULL, NULL);
		return false;
	}
	if(FAILED(D3DXCreateTexture(_device, 2, 20, D3DX_DEFAULT, D3DUSAGE_DYNAMIC,
		D3DFMT_D32, D3DPOOL_DEFAULT, &texCursor)))
	{
		MessageBox(NULL, _T("failed to create texture."), NULL, NULL);
		return false;
	}
	// 글꼴 만들기
	if(FAILED(D3DXCreateFontIndirect(_device, &desc, &font)))
	{
		::MessageBox(0, _T("D3DXCreateFontIndirect() - FAILED"), 0, 0);
		//::PostQuitMessage(0);
		return false;
	}
	return true;
}
bool
MyInputUI::DrawString(CMyIME* ime, LPDIRECT3DDEVICE9 _device,
					  LPD3DXSPRITE _sprCursor, RECT& _rect, DWORD format, D3DCOLOR color)
{
	//rect = _rect;
	ime->setNumMaxChar((_rect.right - _rect.left) / desc.Width);

	font->DrawText(_sprCursor, ime->getString(), -1, &_rect, format, color);	// 글자 그리기

	currTime = static_cast<float>(timeGetTime());
	if(((currTime - lastTime) * 0.001f) >= 0.5f)
	{
		bVisibleCursor = !bVisibleCursor;
		lastTime = currTime;
	}

	long len = ime->getLength();
	RECT rectCursor;
	rectCursor.left = len; rectCursor.right = len + 2;
	rectCursor.top = 0; rectCursor.bottom = rectCursor.top + logFont.lfHeight;

	//_device->SetRenderTarget(0, surCursor);

	sprCursor->Begin(0);
		if(bVisibleCursor)
		{
			if(FAILED(sprCursor->Draw(texCursor, &rectCursor, NULL, &D3DXVECTOR3(
				static_cast<float>(len + _rect.left), static_cast<float>(_rect.top), 1.0f), color)))
			{
				MessageBox(NULL, _T("failed to draw cursor."), NULL, NULL);
				return false;
			}
		}
	sprCursor->End();

	return true;
}