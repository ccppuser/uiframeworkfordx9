#include "MyMsgboxUI.h"
#define FC_GRAY 0xff777777
MyMsgboxUI::MyMsgboxUI(MyUIManager* _mgr)
: mgr(_mgr), MyFrameUI(), MyTextUI(), MyButtonUI(), idxStr(0),
isBttnUsing(0)
//isBttnDown(0), isBttnClicked(0)
{}
MyMsgboxUI::MyMsgboxUI(const MyMsgboxUI &)
{}
MyMsgboxUI::~MyMsgboxUI()
{
	DeleteObject(font_normal);
	DeleteObject(font_bold);
}
MyMsgboxUI&
MyMsgboxUI::operator =(const MyMsgboxUI &)
{
	return *this;
}
bool
MyMsgboxUI::initMsgboxFrame(LPDIRECT3DDEVICE9 _device, D3DXFONT_DESC& _desc,
		TCHAR* frameImage, Frame& frm, int frmX, int frmY, int frmWidth, int frmHeight,
		const TCHAR* normBttnImage, const TCHAR* mouseOnImagePath, const TCHAR* clickedBttnImage,
		int bttnX, int bttnY, int bttnWidth, int bttnHeight, float fontSizeRate)
{
	device = _device;
	if(!initFrame(device, frameImage, frm, frmX, frmY, frmWidth, frmHeight)) return false;

	_desc.Height *= fontSizeRate; _desc.Width *= fontSizeRate;
	if(!initText(device, mgr, _desc)) return false;
	_desc.Height /= fontSizeRate; _desc.Width /= fontSizeRate;

	if(normBttnImage == 0 || clickedBttnImage == 0 || mouseOnImagePath == 0)
	{
		isBttnUsing = false;
	}
	else
	{
		isBttnUsing = true;
		if(!initButton(device, mgr, normBttnImage, mouseOnImagePath, clickedBttnImage, bttnX, bttnY, bttnWidth, bttnHeight))
			return false;
	}

	font_normal = CreateFont(desc.Height, desc.Width, 0, 0, desc.Weight, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, NONANTIALIASED_QUALITY, DEFAULT_PITCH, desc.FaceName);
	font_bold = CreateFont(desc.Height, desc.Width, 0, 0, MAX_WEIGHT, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, NONANTIALIASED_QUALITY, DEFAULT_PITCH, desc.FaceName);

	return true;
}
bool
MyMsgboxUI::drawMsgbox()
{
	if(!drawFrame()) return false;
	if(isBttnUsing)
		drawButton();
	for(int i = 0 ; i < idxStr ; ++i)
	{
		drawText(m_strInfo[i].str, NULL, (int)pos.x + rectFrm[FRM_C].left + 20,
			(int)pos.y + rectFrm[FRM_C].top + (int)(i * desc.Height * 1.2f * m_strInfo[i].rowSize) + desc.Height * 1.6f,
			DT_TOP|DT_LEFT, 0xff777777);
	}
	return true;
}

bool
MyMsgboxUI::drawBoldText(std::string _str, std::string _space, int i)
{
	if(!drawFrame()) return false;
	
	SIZE size;
	HDC hdc = GetDC(NULL);
	HFONT old_font = (HFONT)SelectObject(hdc, font_normal);
	GetTextExtentPoint32(hdc, _space.c_str(), _space.size(), &size);
	SelectObject(hdc, old_font);
	ReleaseDC(NULL, hdc);
	
	int ori_wei = desc.Weight;
	desc.Weight = 1000;
	initText(device, mgr, desc);
	
	if(isBttnUsing)
		drawButton();
	
	drawText(_str, NULL, (int)pos.x + rectFrm[FRM_C].left + 20 + size.cx,
		(int)pos.y + rectFrm[FRM_C].top + (int)(i * desc.Height * 1.2f) + desc.Height * 1.6f,
		DT_TOP|DT_LEFT, 0xffff0000);
	
	desc.Weight = ori_wei;
	initText(device, mgr, desc);
	
	return true;
}

bool
MyMsgboxUI::drawNormalText(std::string _str, std::string _space, int i)
{
	if(!drawFrame()) return false;

	SIZE size;
	HDC hdc = GetDC(NULL);
	HFONT old_font = (HFONT)SelectObject(hdc, font_bold);
	GetTextExtentPoint32(hdc, _space.c_str(), (int)_space.size(), &size);
	SelectObject(hdc, old_font);
	ReleaseDC(NULL, hdc);

	int ori_wei = desc.Weight;
	desc.Weight = 1000;
	initText(device, mgr, desc);

	if(isBttnUsing)
		drawButton();

	drawText(_str, NULL, (int)pos.x + rectFrm[FRM_C].left + 20 + size.cx,
		(int)pos.y + rectFrm[FRM_C].top + (int)(i * desc.Height * 1.2f) + desc.Height * 1.6f,
		DT_TOP|DT_LEFT, 0xff777777);

	desc.Weight = ori_wei;
	initText(device, mgr, desc);

	return true;
}

bool
MyMsgboxUI::drawComplexText(std::string _str, std::string bold_space, std::string nor_space, int i, D3DCOLOR color, float rowSize)
{
	if(!drawFrame()) return false;

	SIZE size_b, size_n;
	HDC hdc = GetDC(NULL);
	HFONT old_font = (HFONT)SelectObject(hdc, font_bold);
	GetTextExtentPoint32(hdc, bold_space.c_str(), (int)bold_space.size(), &size_b); //bold관격
	SelectObject(hdc, font_normal);
	GetTextExtentPoint32(hdc, nor_space.c_str(), (int)nor_space.size(), &size_n); //일반글씨간격
	SelectObject(hdc, old_font);
	ReleaseDC(NULL, hdc);

	int ori_wei = desc.Weight;
	desc.Weight = 1000;
	initText(device, mgr, desc);

	if(isBttnUsing)
		drawButton();

	drawText(_str, NULL, (int)pos.x + rectFrm[FRM_C].left + 20 + size_b.cx + size_n.cx,
		(int)pos.y + rectFrm[FRM_C].top + (int)(i * desc.Height * 1.2f * rowSize) + desc.Height * 1.6f,
		DT_TOP|DT_LEFT, color);

	desc.Weight = ori_wei;
	initText(device, mgr, desc);

	return true;
}

bool
MyMsgboxUI::registText(std::string _str, float _rowSize)
{
	m_strInfo[idxStr].str = _str;
	m_strInfo[idxStr].rowSize = _rowSize;

	if(idxStr > 0)
	{
		if(( m_strInfo[idxStr].rowSize != -1.0f) 
			&& m_strInfo[idxStr-1].rowSize != -1.0f )
			m_strInfo[idxStr].rowSize = m_strInfo[idxStr-1].rowSize;
		else
			m_strInfo[idxStr].rowSize = 1.0f;
	}

	idxStr++;
	return true;
}
bool
MyMsgboxUI::unRegistTexts()
{
	for(int i = 0 ; i < MAX_STRING ; ++i)
		m_strInfo[i].str.clear();
	idxStr = 0;
	return true;
}
bool
MyMsgboxUI::isButtonClicked()
{
	if(isBttnUsing)
	{
		if(mgr->getMouseState() == MS_LBDOWN && isMouseOnButton(mgr->getMousePos()))
		{
			isBttnDown = true;
		}
		else
		{
			if(isBttnDown && isMouseOnButton(mgr->getMousePos()))
			{
				isBttnClicked = true;
			}
			isBttnDown = false;
		}
	}
	if(isBttnClicked)
	{
		isBttnClicked = false;
		return true;
	}
	return false;
}