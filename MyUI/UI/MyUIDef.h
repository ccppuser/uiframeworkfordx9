#pragma once

#include <tchar.h>

#include <iostream>
#include <d3d9.h>
#include <d3dx9.h>
#include <mmsystem.h>

#define CHECK_ERROR(x, msg) if(!x){ MessageBox(NULL, msg, NULL, NULL); return G_FAIL; }
#ifndef SAFE_DELETE
#define SAFE_DELETE(x)			if(x){delete x; x = 0;}
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(x)	if(x){delete [] x; x = 0;}
#endif
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x)			if(x){x->Release(); x = 0;}
#endif

#define TESTX 1.006f
#define TESTY 1.04f
#define TESTXX 3.0f
#define TESTYY 28.0f

#if (defined _UNICODE && defined UNICODE)
	#define String std::wstring
#else
	#define String std::string
#endif

// 프레임을 구성하는 요소의 실제 이미지상 좌표 구조체
struct Frame
{
	int out_l, out_t, out_w, out_h,
		in_l, in_t, in_w, in_h;
	Frame(){}
	Frame(int _out_l, int _out_t, int _out_w, int _out_h,
		int _in_l, int _in_t, int _in_w, int _in_h)
	{
		out_l = _out_l; out_t = _out_t; out_w = _out_w; out_h = _out_h;
		in_l = _in_l; in_t = _in_t; in_w = _in_w; in_h = _in_h;
	}
};

// 왼쪽/오른쪽 마우스 버튼 눌렀나 안눌렀나 상태 타입
typedef enum MouseState{ MS_IDLE, MS_LBDOWN, MS_LBUP, MS_RBDOWN, MS_RBUP };
// 키보드 입력 상태(아무 키) 타입
typedef enum KeyState{ KS_IDLE, KS_DOWN };

// 마우스 좌표 구조체
struct MousePos
{
	long x, y;
	MousePos(){}
	MousePos(long _x, long _y)
	{ x = _x; y = _y; }
};