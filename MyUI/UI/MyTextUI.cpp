#include "MyTextUI.h"

MyTextUI::MyTextUI(void)
: font(0), isTxtDown(0), isTxtClicked(0)
{}
MyTextUI::MyTextUI(const MyTextUI&)
{}
MyTextUI::~MyTextUI(void)
{
	SAFE_RELEASE(font);
}
MyTextUI&
MyTextUI::operator =(const MyTextUI&)
{
	return *this;
}
D3DXFONT_DESC
MyTextUI::getDesc(){
	return desc;
}
bool
MyTextUI::initText(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr, D3DXFONT_DESC& _desc)
{
	if(font)
		SAFE_RELEASE(font);
	mgr = _mgr;
	desc = _desc;
	if(!device)
		return false;	// D3D디바이스 생성 안돼있으면 false

	LOGFONT lf;
	ZeroMemory(&lf, sizeof(lf));

	lf.lfCharSet		= _desc.CharSet;
	lf.lfHeight			= _desc.Height;
	lf.lfItalic			= _desc.Italic;
	lf.lfOutPrecision	= _desc.OutputPrecision;
	lf.lfPitchAndFamily	= _desc.PitchAndFamily;
	lf.lfQuality		= _desc.Quality;
	lf.lfWeight			= _desc.Weight;
	lf.lfWidth			= _desc.Width;
	logFont = lf;

	// 글꼴 만들기
	if(FAILED(D3DXCreateFontIndirect(device, &desc, &font)))
	{
		::MessageBox(0, _T("D3DXCreateFontIndirect() - FAILED"), 0, 0);
		return false;
	}
	return true;
}
bool
MyTextUI::drawText(std::string _str, LPD3DXSPRITE _spr, int x, int y, DWORD format, D3DCOLOR color)
{
	int outLineOffset = desc.Width / 12;
	str = _str;

	rectText.left = x; rectText.top = y;
	rectText.right = x + getLength() + desc.Width * 5; rectText.bottom = y + desc.Height;
	font->DrawText(_spr, _str.c_str(), -1, &rectText, format, color);
	return true;
}
long
MyTextUI::getLength()
{
	HWND hwnd = GetWindow(NULL, 0);
	HDC hdc = GetDC(hwnd);
	HFONT hFont = CreateFontIndirect(&logFont);
	HFONT holdFont;
	SIZE sz;
	long len;

	holdFont = (HFONT)SelectObject(hdc, hFont);

	// 문자열의 실제 윈도우상 길이를 구함
	GetTextExtentPoint32(hdc, str.c_str(), (int)strlen(str.c_str()), &sz);
	len = sz.cx;

	SelectObject(hdc, holdFont);
	DeleteObject(hFont);
	ReleaseDC(hwnd, hdc);

	return len;
}
bool
MyTextUI::isMouseOnText(MousePos& pos)
{
	if(!mgr->isWindowMode())
	{
		if(pos.x >= rectText.left - TESTXX &&
			pos.x <= rectText.right - TESTXX &&
			pos.y >= rectText.top - TESTYY &&
			pos.y <= rectText.bottom - TESTYY)
		{
			return TRUE;
		}
	}
	else if(pos.x >= rectText.left / TESTX &&
		pos.x <= rectText.right / TESTX &&
		pos.y >= rectText.top / TESTY &&
		pos.y <= rectText.bottom / TESTY)
	{
		return TRUE;
	}
	return FALSE;
}
bool
MyTextUI::isTextClicked()
{
	if(mgr->getMouseState() == MS_LBDOWN && isMouseOnText(mgr->getMousePos()))
	{
		isTxtDown = true;
	}
	else
	{
		if(isTxtDown && isMouseOnText(mgr->getMousePos()))
		{
			isTxtDown = false;
			isTxtClicked = true;
		}
	}
	if(isTxtClicked)
	{
		isTxtClicked = false;
		return true;
	}
	return false;
}