//
// 설명 : 빈 창 UI 하나를 정의하는 클래스
//

#pragma once

#include "MyUIDef.h"

class MyFrameUI
{
protected:
	// 창 부분 마다 ID 부여
	// FRM_LT(left top), FRM_RT(right top), FRM_RB(right bottom), FRM_LB(left bottom)
	// FRM_T(top), FRM_R(right), FRM_B(bottom), FRM_L(left), FRM_C(center)
	enum{
		FRM_LT, FRM_RT, FRM_RB, FRM_LB,
		FRM_T, FRM_R, FRM_B, FRM_L,
		FRM_C
	};
	LPD3DXSPRITE		sprFrame;
	LPDIRECT3DTEXTURE9	texFrame;

	D3DXVECTOR3			pos;
	RECT				rectFrm[9];
	Frame				offset;

	float				scaleWidth, oldScaleWidth;
	float				scaleHeight, oldScaleHeight;
	bool				isScaled;

public:
	// 생성자 및 소멸자
	MyFrameUI();
	MyFrameUI(const MyFrameUI& rhs);
	virtual ~MyFrameUI();

	// 연산자
	MyFrameUI& operator = (const MyFrameUI& rhs);

	// 창 초기화(실패하면 false)	(d3d디바이스, 텍스쳐 이름, 그려질 left-top위치)
	bool initFrame(LPDIRECT3DDEVICE9 _device, TCHAR* fileName, Frame& frm, int x, int y, int width, int height);

	// 창 그리기(실패하면 false)
	// BeginScene() 와 EndScene() 사이에서 호출해야 함(d3d디바이스)
	bool drawFrame();

	// 창 늘이기
	void scale(float _scaleWidth, float _scaleHeight);
};