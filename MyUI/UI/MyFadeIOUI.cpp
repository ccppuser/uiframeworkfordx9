#include "MyFadeIOUI.h"

MyFadeIOUI::MyFadeIOUI()
: sprFade(0), texFade(0),
alphaIn(0xff000000), oldAlphaIn(0xff000000), alphaOut(0x00000000), oldAlphaOut(0x00000000),
timeLastIn(0.0f), timeElapsedIn(0.0f), timeLastOut(0.0f), timeElapsedOut(0.0f),
isFinishedIn(false), isFinishedOut(false)
{
	rectFade.left = 0; rectFade.top = 0;
	rectFade.right = 0; rectFade.bottom = 0;
}
MyFadeIOUI::MyFadeIOUI(const MyFadeIOUI &)
{}
MyFadeIOUI::~MyFadeIOUI()
{
	SAFE_RELEASE(sprFade);
	SAFE_RELEASE(texFade);
}
MyFadeIOUI&
MyFadeIOUI::operator =(const MyFadeIOUI&)
{
	return *this;
}
BOOL
MyFadeIOUI::initFade(LPDIRECT3DDEVICE9 device, int _width, int _height)
{
	width = _width;
	height = _height;
	if(FAILED(D3DXCreateSprite(device, &sprFade)))
	{
		MessageBox(NULL, "failed to create sprite", NULL, NULL);
		return FALSE;
	}
	if(FAILED(D3DXCreateTexture(device, _width, _height, D3DX_DEFAULT, D3DUSAGE_DYNAMIC,
		D3DFMT_D32, D3DPOOL_DEFAULT, &texFade)))
	{
		MessageBox(NULL, _T("failed to create texture."), NULL, NULL);
		return false;
	}
	return TRUE;
}
VOID
MyFadeIOUI::initFadeIn()
{
	alphaIn = 0xff000000;
	oldAlphaIn = 0xff000000;
	timeLastIn = 0.0f;
	timeElapsedIn = 0.0f;
	isFinishedIn = false;
	rectFade.left = 0; rectFade.top = 0;
	rectFade.right = 0; rectFade.bottom = 0;
}
VOID
MyFadeIOUI::initFadeOut()
{
	alphaOut = 0x00000000;
	oldAlphaOut = 0x00000000;
	timeLastOut = 0.0f;
	timeElapsedOut = 0.0f;
	isFinishedOut = false;
	rectFade.left = 0; rectFade.top = 0;
	rectFade.right = 0; rectFade.bottom = 0;
}
BOOL
MyFadeIOUI::fadeIn(float speed)
{
	rectFade.right = width; rectFade.bottom = height;

	float timeCurr = 0.0f;
	timeCurr = static_cast<float>(timeGetTime());
	float timeDelta = (timeCurr - timeLastIn) * 0.001f;
	timeElapsedIn += timeDelta;
	if(!isFinishedIn)
	{
		if(timeElapsedIn >= 0.01666666f && speed < 0.1f)	// �����ӷ� ���� ��
		{
			alphaIn -= D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, speed);
			timeElapsedIn = 0.0f;
		}
		else if(timeElapsedIn >= 0.01666666f && speed < 1.0f)	// �����ӷ� ���� ��
		{
			alphaIn -= D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, speed * 0.1f);
			timeElapsedIn = 0.0f;
		}
	}
	if(oldAlphaIn < alphaIn)
	{
		isFinishedIn = true;
		return TRUE;
	}
	oldAlphaIn = alphaIn;
	timeLastIn = timeCurr;
	sprFade->Begin(0);
	sprFade->Draw(texFade, &rectFade, 0, &D3DXVECTOR3(0.0f,0.0f,0.0f), alphaIn);
	sprFade->End();
	return FALSE;
}
BOOL
MyFadeIOUI::fadeOut(float speed)
{
	rectFade.right = width; rectFade.bottom = height;

	float timeCurr = 0.0f;
	timeCurr = static_cast<float>(timeGetTime());
	float timeDelta = (timeCurr - timeLastOut) * 0.001f;
	timeElapsedOut += timeDelta;
	if(!isFinishedOut)
	{
		if(timeElapsedOut >= 0.01666666f && speed < 0.1f)	// �����ӷ� ���� ��
		{
			alphaOut += D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, speed);
			timeElapsedOut = 0.0f;
		}
		else if(timeElapsedOut >= 0.01666666f && speed < 1.0f)	// �����ӷ� ���� ��
		{
			alphaOut += D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, speed * 0.1f);
			timeElapsedOut = 0.0f;
		}
	}
	if(oldAlphaOut > alphaOut)	// ���̵� �ƿ� ������ ���� ȭ��
	{
		RECT r = {0, 0, width, height};
		sprFade->Begin(0);
		sprFade->Draw(texFade, &r, 0, &D3DXVECTOR3(0.0f,0.0f,0.0f), 0xff000000);
		sprFade->End();
		isFinishedOut = true;
		return TRUE;
	}
	else
	{
		oldAlphaOut = alphaOut;
		timeLastOut = timeCurr;
		sprFade->Begin(0);
		sprFade->Draw(texFade, &rectFade, 0, &D3DXVECTOR3(0.0f,0.0f,0.0f), alphaOut);
		sprFade->End();
		return FALSE;
	}
}