#include "MyFrameUI.h"

MyFrameUI::MyFrameUI()
: sprFrame(0), texFrame(0), pos(0.0f, 0.0f, 0.0f), scaleWidth(1.0f), scaleHeight(1.0f),
isScaled(0)
{}
MyFrameUI::MyFrameUI(const MyFrameUI& rhs)
{}
MyFrameUI::~MyFrameUI()
{
	SAFE_RELEASE(sprFrame);
	SAFE_RELEASE(texFrame);
}
MyFrameUI&
MyFrameUI::operator = (const MyFrameUI& rhs)
{
	sprFrame = 0;
	texFrame = 0;
	pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	scaleWidth = 1.0f;
	scaleHeight = 1.0f;
	return *this;
}
bool
MyFrameUI::initFrame(LPDIRECT3DDEVICE9 _device, TCHAR* fileName, Frame& frm, int x, int y, int width, int height)
{
	// 프레임 출력용 스프라이트
	if(FAILED(D3DXCreateSprite(_device, &sprFrame)))
	{
		MessageBox(NULL, _T("failed to create sprite."), NULL, NULL);
		return false;
	}
	// 프레임을 정의하는 텍스쳐
	if(FAILED(D3DXCreateTextureFromFileEx(_device, fileName, width, height, D3DX_FROM_FILE,
		D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT,
		D3DX_DEFAULT, 0xff00ffff, NULL, NULL, &texFrame)))
	{
		MessageBox(NULL, _T("failed to create texture."), NULL, NULL);
		return false;
	}
	pos.x = (float)x; pos.y = (float)y; pos.z = 0.0f;

	//frame temp = {0, 0, 640, 480, 12, 26, 615, 373};
	//frame temp = {128, 0, 128, 64, 136, 9, 112, 46};
	//frame temp = {0, 0, 512, 512, 12, 26, 500, 486};
	offset = frm;

	rectFrm[FRM_LT].left = frm.out_l;				rectFrm[FRM_LT].right = frm.in_l;
	rectFrm[FRM_LT].top = frm.out_t;				rectFrm[FRM_LT].bottom = frm.in_t;

	rectFrm[FRM_RT].left = frm.in_l + frm.in_w;	rectFrm[FRM_RT].right = frm.out_l + frm.out_w;
	rectFrm[FRM_RT].top = frm.out_t;				rectFrm[FRM_RT].bottom = frm.in_t;

	rectFrm[FRM_RB].left = frm.in_l + frm.in_w;	rectFrm[FRM_RB].right = frm.out_l + frm.out_w;
	rectFrm[FRM_RB].top = frm.in_t + frm.in_h;		rectFrm[FRM_RB].bottom = frm.out_h;

	rectFrm[FRM_LB].left = frm.out_l;				rectFrm[FRM_LB].right = frm.in_l;
	rectFrm[FRM_LB].top = frm.in_t + frm.in_h;		rectFrm[FRM_LB].bottom = frm.out_h;

	rectFrm[FRM_L].left = frm.out_l;				rectFrm[FRM_L].right = frm.in_l;
	rectFrm[FRM_L].top = frm.in_t;					rectFrm[FRM_L].bottom = frm.in_t + frm.in_h;

	rectFrm[FRM_T].left = frm.in_l;				rectFrm[FRM_T].right = frm.in_l + frm.in_w;
	rectFrm[FRM_T].top = frm.out_t;				rectFrm[FRM_T].bottom = frm.in_t;

	rectFrm[FRM_R].left = frm.in_l + frm.in_w;		rectFrm[FRM_R].right = frm.out_l + frm.out_w;
	rectFrm[FRM_R].top = frm.in_t;					rectFrm[FRM_R].bottom = frm.in_t + frm.in_h;

	rectFrm[FRM_B].left = frm.in_l;				rectFrm[FRM_B].right = frm.in_l + frm.in_w;
	rectFrm[FRM_B].top = frm.in_t + frm.in_h;		rectFrm[FRM_B].bottom = frm.out_h;

	rectFrm[FRM_C].left = frm.in_l;				rectFrm[FRM_C].right = frm.in_l + frm.in_w;
	rectFrm[FRM_C].top = frm.in_t;					rectFrm[FRM_C].bottom = frm.in_t + frm.in_h;
	return true;
}
bool
MyFrameUI::drawFrame()
{
	sprFrame->Begin(0);
		// 스케일 매트릭스 생성
		D3DXMATRIX matScaleW, matScaleH;
		D3DXMatrixIdentity(&matScaleW);
		D3DXMatrixIdentity(&matScaleH);
		D3DXMatrixScaling(&matScaleW, scaleWidth, 1.0f, 1.0f);
		D3DXMatrixScaling(&matScaleH, 1.0f, scaleHeight, 1.0f);
		//// LT
		//sprFrame->SetTransform(D3DXMatrixScaling(&D3DXMATRIX(), 1.0f, 1.0f, 1.0f));	// 스케일 매트릭스 적용
		//sprFrame->Draw(texFrame, &rectFrm[FRM_LT], NULL,
		//	&(pos+D3DXVECTOR3((float)(rectFrm[FRM_LT].left - offset.out_l) / scaleWidth,
		//	(float)(rectFrm[FRM_LT].top - offset.out_t) / scaleHeight, 0.0f)), 0xffffffff);
		//// RT
		//sprFrame->SetTransform(D3DXMatrixScaling(&D3DXMATRIX(), 1.0f, 1.0f, 1.0f));
		//sprFrame->Draw(texFrame, &rectFrm[FRM_RT], NULL,
		//	&(pos+D3DXVECTOR3((float)(rectFrm[FRM_RT].left - offset.out_l) * scaleWidth - (scaleWidth - 1) * 8.0f,
		//	(float)(rectFrm[FRM_RT].top - offset.out_t) / scaleHeight, 0.0f)), 0xffffffff);
		//// RB
		//sprFrame->SetTransform(D3DXMatrixScaling(&D3DXMATRIX(), 1.0f, 1.0f, 1.0f));
		//sprFrame->Draw(texFrame, &rectFrm[FRM_RB], NULL,
		//	&(pos+D3DXVECTOR3((float)(rectFrm[FRM_RB].left - offset.out_l) * scaleWidth - (scaleWidth - 1.0f) * 8.0f,
		//	(float)(rectFrm[FRM_RB].top - offset.out_t) * scaleHeight - (scaleHeight - 1.0f) * 9.0f, 0.0f)), 0xffffffff);
		//// LB
		//sprFrame->SetTransform(D3DXMatrixScaling(&D3DXMATRIX(), 1.0f, 1.0f, 1.0f));
		//sprFrame->Draw(texFrame, &rectFrm[FRM_LB], NULL,
		//	&(pos+D3DXVECTOR3((float)(rectFrm[FRM_LB].left - offset.out_l) / scaleWidth,
		//	(float)(rectFrm[FRM_LB].top - offset.out_t) * scaleHeight - (scaleHeight - 1.0f) * 9.0f, 0.0f)), 0xffffffff);
		//// T
		//sprFrame->SetTransform(&matScaleW);
		//sprFrame->Draw(texFrame, &rectFrm[FRM_T], NULL,
		//	&(D3DXVECTOR3((float)(rectFrm[FRM_T].left - offset.out_l) / scaleWidth + pos.x / scaleWidth,
		//	(float)(rectFrm[FRM_T].top - offset.out_t) / scaleHeight + pos.y, 0.0f)), 0xffffffff);
		//// R
		//sprFrame->SetTransform(&matScaleH);
		//sprFrame->Draw(texFrame, &rectFrm[FRM_R], NULL,
		//	&(D3DXVECTOR3((float)(rectFrm[FRM_R].left - offset.out_l) * scaleWidth - (scaleWidth - 1.0f) * 8.0f + pos.x,
		//	(float)(rectFrm[FRM_R].top - offset.out_t) / scaleHeight + pos.y / scaleHeight, 0.0f)), 0xffffffff);
		//// B
		//sprFrame->SetTransform(&matScaleW);
		//sprFrame->Draw(texFrame, &rectFrm[FRM_B], NULL,
		//	&(D3DXVECTOR3((float)(rectFrm[FRM_B].left - offset.out_l) / scaleWidth + pos.x / scaleWidth,
		//	(float)(rectFrm[FRM_B].top - offset.out_t) * scaleHeight - (scaleHeight - 1.0f) * 9.0f + pos.y, 0.0f)), 0xffffffff);
		//// L
		//sprFrame->SetTransform(&matScaleH);
		//sprFrame->Draw(texFrame, &rectFrm[FRM_L], NULL,
		//	&(D3DXVECTOR3((float)(rectFrm[FRM_L].left - offset.out_l) / scaleWidth + pos.x,
		//	(float)(rectFrm[FRM_L].top - offset.out_t) / scaleHeight + pos.y / scaleHeight, 0.0f)), 0xffffffff);
		//// C
		//sprFrame->SetTransform(&(matScaleW * matScaleH));
		//sprFrame->Draw(texFrame, &rectFrm[FRM_C], NULL,
		//	&(D3DXVECTOR3((float)(rectFrm[FRM_C].left - offset.out_l) / scaleWidth + pos.x / scaleWidth,
		//	(float)(rectFrm[FRM_C].top - offset.out_t) / scaleHeight + pos.y / scaleHeight, 0.0f)), 0xffffffff);
		for(int i = 0 ; i < 9 ; ++i)
		{
		sprFrame->Draw(texFrame, &rectFrm[i], NULL,
			&(pos+D3DXVECTOR3((float)(rectFrm[i].left - offset.out_l),
			(float)(rectFrm[i].top - offset.out_t), 1.0f)), 0xffffffff);
		}
	sprFrame->End();

	oldScaleWidth = scaleWidth;
	oldScaleHeight = scaleHeight;
	isScaled = false;
	return true;
}
void
MyFrameUI::scale(float _scaleWidth, float _scaleHeight)
{
	scaleWidth = _scaleWidth;
	scaleHeight = _scaleHeight;
	isScaled = true;
}