#pragma once

#include "MyUIDef.h"

class MyUIManager
{
protected:
	MouseState	mouseState;
	MouseState	oldMouseState;
	MousePos	mousePos;

	KeyState	keyState;

	bool		isLeftDowned[4];
	bool		isLeftClicked[4];

	bool		isWinMode;

	int			num;

public:
	// 생성자 및 소멸자
	MyUIManager();
	MyUIManager(const MyUIManager&);
	virtual ~MyUIManager();

	// 연산자
	MyUIManager& operator = (const MyUIManager&);

	// 창/전체화면 모드 설정
	void		setWindowMode(bool _bWindow);

	// 창모드 인가?
	bool		isWindowMode();

	// 마우스 상태를 매니져에 넘김
	void		sendMouseState(MouseState ms);

	// 마우스 좌표를 매니져에 넘김
	void		sendMousePos(MousePos& mp);

	// 키보드 상태를 매니져에 넘김
	void		keyDownedInWndProc();

	// 마우스 상태 리턴
	MouseState	getMouseState();

	// 마우스 좌표 리턴
	MousePos	getMousePos();

	// 마우스 왼쪽 클릭이 일어났는가
	// (rect 범위 포인터를 보내면 rect 안에서 클릭할 때만 검사, NULL 보내면 full-screen 검사)
	bool		isLeftMouseButtonClicked(RECT* rect, int _num = 0);

	// 아무 키가 눌렸는지 여부를 리턴
	bool		isAnyKeyDown();
};