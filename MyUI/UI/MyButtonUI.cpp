#include "MyButtonUI.h"

MyButtonUI::MyButtonUI()
: sprButton(0), texNormal(0), /*sprMouseOn(0), */texMouseOn(0), /*sprClicked(0), */texClicked(0),
/*selfManage(0), */isBttnDown(0), isBttnClicked(0)
{}
MyButtonUI::MyButtonUI(const MyButtonUI&)
{}
MyButtonUI::~MyButtonUI()
{
	SAFE_RELEASE(sprButton);
	SAFE_RELEASE(texNormal);
	//SAFE_RELEASE(sprMouseOn);
	SAFE_RELEASE(texMouseOn);
	//SAFE_RELEASE(sprClicked);
	SAFE_RELEASE(texClicked);
}
MyButtonUI&
MyButtonUI::operator = (const MyButtonUI&)
{
	return *this;
}
BOOL
MyButtonUI::initButton(LPDIRECT3DDEVICE9 device, MyUIManager* _mgr,
					   const TCHAR* normalImagePath, const TCHAR* mouseOnImagePath, const TCHAR* clickedImagePath,
					   int _x, int _y, int _width, int _height)
{
	mgr = _mgr;
	rectButton.left = _x; rectButton.top = _y;
	rectButton.right = _x + _width; rectButton.bottom = _y + _height;

	// IDLE 상태 버튼 스프라이트 및 텍스쳐 로드
	if(FAILED(D3DXCreateSprite(device, &sprButton)))
	{
		MessageBox(NULL, "failed to create sprite.", NULL, NULL);
		return FALSE;
	}
	if(FAILED(D3DXCreateTextureFromFileEx(device, normalImagePath, _width, _height,
		D3DX_DEFAULT, D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		0, NULL, NULL, &texNormal)))
	{
		MessageBox(NULL, "failed to create texture.", NULL, NULL);
		return FALSE;
	}

	// Mouse On 상태 버튼 스프라이트 및 텍스쳐 로드
	/*if(FAILED(D3DXCreateSprite(device, &sprMouseOn)))
	{
		MessageBox(NULL, "failed to create sprite.", NULL, NULL);
		return FALSE;
	}*/
	if(FAILED(D3DXCreateTextureFromFileEx(device, mouseOnImagePath, _width, _height,
		D3DX_DEFAULT, D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		0, NULL, NULL, &texMouseOn)))
	{
		MessageBox(NULL, "failed to create texture.", NULL, NULL);
		return FALSE;
	}

	// 눌린 상태 버튼 스프라이트 및 텍스쳐 로드
	/*if(FAILED(D3DXCreateSprite(device, &sprClicked)))
	{
		MessageBox(NULL, "failed to create sprite.", NULL, NULL);
		return FALSE;
	}*/
	if(FAILED(D3DXCreateTextureFromFileEx(device, clickedImagePath, _width, _height,
		D3DX_DEFAULT, D3DUSAGE_DYNAMIC, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		0, NULL, NULL, &texClicked)))
	{
		MessageBox(NULL, "failed to create texture.", NULL, NULL);
		return FALSE;
	}
	return TRUE;
}
void
MyButtonUI::registManager(MyUIManager* _mgr)
{
	mgr = _mgr;
}
BOOL
MyButtonUI::drawNormalButton()
{
	D3DXVECTOR3 pos((float)rectButton.left, (float)rectButton.top, 1.0f);
	sprButton->Begin(0);
		sprButton->Draw(texNormal, NULL, NULL, &pos, 0xffffffff);
	sprButton->End();
	return TRUE;
}
BOOL
MyButtonUI::drawMouseOnButton()
{
	D3DXVECTOR3 pos((float)rectButton.left, (float)rectButton.top, 1.0f);
	sprButton->Begin(0);
		sprButton->Draw(texMouseOn, NULL, NULL, &pos, 0xffffffff);
	sprButton->End();
	return TRUE;
}
BOOL
MyButtonUI::drawClickedButton()
{
	D3DXVECTOR3 pos((float)rectButton.left, (float)rectButton.top, 1.0f);
	sprButton->Begin(0);
		sprButton->Draw(texClicked, NULL, NULL, &pos, 0xffffffff);
	sprButton->End();
	return TRUE;
}
BOOL
MyButtonUI::drawButton()
{
	if(!isBttnDown)
	{
		if(isMouseOnButton(mgr->getMousePos()))
			return drawMouseOnButton();
		return drawNormalButton();
	}
	else
		return drawClickedButton();
	return false;
}
BOOL
MyButtonUI::isMouseOnButton(MousePos& pos)
{
	if(!mgr->isWindowMode())
	{
		if(pos.x >= rectButton.left - TESTXX &&
			pos.x <= rectButton.right - TESTXX &&
			pos.y >= rectButton.top - TESTYY &&
			pos.y <= rectButton.bottom - TESTYY)
		{
			return TRUE;
		}
	}
	else if(pos.x >= rectButton.left / TESTX &&
		pos.x <= rectButton.right / TESTX &&
		pos.y >= rectButton.top / TESTY &&
		pos.y <= rectButton.bottom / TESTY)
	{
		return TRUE;
	}
	return FALSE;
}
BOOL
MyButtonUI::isButtonClicked()
{
	if(mgr->getMouseState() == MS_LBDOWN && isMouseOnButton(mgr->getMousePos()))
	{
		isBttnDown = true;
	}
	else
	{
		if(isBttnDown && isMouseOnButton(mgr->getMousePos()))
		{
			isBttnClicked = true;
		}
		isBttnDown = false;
	}
	if(isBttnClicked)
	{
		isBttnClicked = false;
		return true;
	}
	return false;
}