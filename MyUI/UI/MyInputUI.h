//
// 설명 : 지정된 스프라이트에 키보드 입력 UI를 삽입하는 클래스
//

#pragma once

#include "MyUIDef.h"
#include "../IME/MyIME.h"

class MyInputUI
{
protected:
	LOGFONT				logFont;
	LPD3DXFONT			font;
	LPD3DXSPRITE		sprCursor;
	LPDIRECT3DTEXTURE9	texCursor;
	D3DXFONT_DESC		desc;
	//RECT				rect;

	bool	bVisibleCursor;
	float	lastTime;
	float	currTime;

public:
	// 생성자 및 소멸자
	MyInputUI(void);
	MyInputUI(const MyInputUI& rhs);
	virtual ~MyInputUI(void);

	// 연산자
	MyInputUI& operator = (const MyInputUI& rhs);

public:
	// d3d디바이스와 글꼴로 키입력 UI를 초기화
	bool initInputUI(LPDIRECT3DDEVICE9 _device, D3DXFONT_DESC& _desc);

	// 현재 문자열을 그려줌(그릴 d3d스프라이트, 그릴 RECT 객체, rect에서의 그릴 위치, 글자색 필요)
	// BeginScene() 와 EndScene() 사이에서 호출해야 함(d3d디바이스)
	bool DrawString(CMyIME* ime, LPDIRECT3DDEVICE9 _device,
		LPD3DXSPRITE _sprCursor, RECT& _rect, DWORD format, D3DCOLOR color);

	// 깜박이는 커서 그려줌(현재 입력중인 문자열 뒤에)
	// BeginScene() 와 EndScene() 사이에서 호출해야 함(d3d디바이스)
	//bool DrawCursor(LPDIRECT3DDEVICE9 _device, long len);
};
